#include <iostream>
#include <vector>

using namespace std;

class Resistencias {
private:
    vector<double> resistencias;

public:
    void ingresarResistencias() {
        cout << "Ingrese los valores de las resistencias. Ingrese un valor negativo para terminar." << endl;
        double valor;
        while (true) {
            cin >> valor;
            if (valor < 0) break;
            resistencias.push_back(valor);
        }
    }

    double equivalenteSerie() {
    double resistencia_total = 0;
    for (size_t i = 0; i < resistencias.size(); ++i) {
        resistencia_total += resistencias[i];
    }
    return resistencia_total;
}

    double equivalenteParalelo() {
    double resistencia_total = 0;
    for (size_t i = 0; i < resistencias.size(); ++i) {
        resistencia_total += 1 / resistencias[i];
    }
    return 1 / resistencia_total;
}

   void mostrarResultado(const string& tipo_calculo, double resultado) {
    cout << "\nResistencias ingresadas: ";
    for (size_t i = 0; i < resistencias.size(); ++i) {
        cout << resistencias[i] << " ";
    }
    cout << "\nResistencia equivalente en " << tipo_calculo << ": " << resultado << " ohms" << endl;
}

};

int main() {
    Resistencias calculadora;

    cout << "�Calcular en serie o en paralelo? ";
    string tipo_calculo;
    cin >> tipo_calculo;

    calculadora.ingresarResistencias();

    double resultado;
    if (tipo_calculo == "serie") {
        resultado = calculadora.equivalenteSerie();
    } else if (tipo_calculo == "paralelo") {
        resultado = calculadora.equivalenteParalelo();
    } else {
        cout << "Opci�n no v�lida." << endl;
        return 1;
    }

    calculadora.mostrarResultado(tipo_calculo, resultado);

    return 0;
}


